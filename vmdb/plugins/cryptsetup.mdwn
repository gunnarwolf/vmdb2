Step: cryptsetup
-----------------------------------------------------------------------------

Set up disk encryption using LUKS with the `cryptsetup` utility. The
encryption passphrase is read from a file or from the output of a
command. The encrypted disk gets opened and can be mounted using a
separate tag for the cleartext view.

Step keys:

* `cryptsetup` &mdash; REQUIRED; the tag for the encrypted block
  device. This is not directly useable by users, or mountable.

* `name` &mdash; REQUIRED; the tag for the de-crypted block device.
  This is what gets mounted and visible to users.

* `password` &mdash; OPTIONAL; the encryption password

* `key-file` &mdash; OPTIONAL; file from where passphrase is read.

* `key-cmd` &mdash; OPTIONAL; command to run, passphrase is the first
  line of its standard output.

One of `password`, `key-file`, or `key-cmd` is REQUIRED.

Example (in the .vmdb file):

    - cryptsetup: cleartext_pv0
      password: hunter2
      name: pv0
