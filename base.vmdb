# This is a sample vmdb2 input file for testing vmdb2 with a few
# different configurations. The check-one script will modify this
# based on its command line options, before building an image.

steps:
  - mkimg: "{{ output }}"
    size: 4G

  - mklabel: msdos
    device: "{{ output }}"

  # Create an EFI partition. It'll not be used if we install GRUB for
  # UEFI, but an extra partition doesn't break anything, and the waste
  # of space doesn't matter for a test image.
  - mkpart: primary
    device: "{{ output }}"
    start: 0%
    end: 1G
    tag: efi

  - mkpart: primary
    device: "{{ output }}"
    start: 1G
    end: 100%
    tag: /

  - kpartx: "{{ output }}"

  - mkfs: vfat
    partition: efi

  - mkfs: ext4
    partition: /

  - mount: /

  - unpack-rootfs: /

  - debootstrap: buster
    mirror: http://deb.debian.org/debian
    variant: minbase
    target: /
    unless: rootfs_unpacked
    include:
      - python3
      - systemd-sysv

  - virtual-filesystems: /

  - cache-rootfs: /
    unless: rootfs_unpacked

  - fstab: /

  - ansible: /
    playbook: ansible.yml

  - grub: uefi
    tag: /
    efi: efi
    console: serial
